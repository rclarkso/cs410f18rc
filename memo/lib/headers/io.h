/*
 * Read for a file or stream data in via an input that is given to
 * the program, then reading into a tmp_buffer to be sorted and
 * processed in the messages queue.
 */

#ifndef IO_H
#define IO_H

#define DEF_SZ 1024

char *create_buffer(int size) {
        /*
         * This method will initalize a buffer
         * dynamically allocated in order to hold
         * the data before distributing it to the
         * queue
         */
        if(size != NULL) {
                size = size;
        } else {
                size = DEF_SZ;
        }

        char *buffer = (char*)malloc(size * sizeof(char));
        printf("Size is %d:", size);

        printf("Placeholder!");
        return buffer;
}

int check_size(char *buffer, size_t buff_sz) {
        /*
         * This will check the size of buffer
         * returning 0 if the size is ok
         * but if not it will return 1
         * then parent method will call
         * expand_buffer
         *
         */
        if(buff_sz < DEF_SZ) {
                return 0;
        }
        else {
                return 1;
        }
}

char * expand_buffer(char *buffer, size_t buff_sz) {
        /*
         * This method will expand the buffer in the case
         * thet the buffer becomes full. It uses the
         * common technique of multiplying the buffer by 
         * 2 and then returning it.
         */
        buffer = (char*)realloc(buffer, (buff_sz*2 * sizeof(char*))); // Read buffer
        return buffer;

}

int write_buffer_file(FILE *fp, char * buffer, size_t buf_sz) {
    /*
     * Takes in a file, reads it into the
     * buffer that has been made.
     *
     */
    return 0;
}

int write_buffer() {
        printf("Placeholder!");
        return 0;
}

int clear_buffer() {
        printf("Placeholder");
        return 1;
}

void file_to_buffer() {
        printf("File to buffer");
}

void stdin_to_buffer() {
        printf("Standard Input to Buffer.");
}

#endif
