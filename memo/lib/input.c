/* 
 * Methods written to mimic processes
 * and then from there, will be invoked by
 * main method to operate accordingly
 *
 */

#include<stdio.h>
#include<stdlib.h>

#include "headers/io.h"

int check_file() {
        // TODO: Add the ability to check for CLA from
        // the optarg
        FILE *fp = fopen("test.txt", "r");
        if(fp != NULL) {
               fseek(fp, 0L, SEEK_END); //Get to the end of file
               int size = ftell(fp);
               char *buffer = create_buffer(size);
               return 0;
        }
        else  {
                printf("File does not exist. Exiting.\n");
                return 1;
        }

}

int main()
{
        check_file();
        return 0;
}
