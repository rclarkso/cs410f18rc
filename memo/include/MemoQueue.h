#ifndef MEMOQUEUE_H
#define MEMOQUEUE_H

#include <FolderName.h>
#include <thread>
#include <mutex>
#include <Message.h>

/**
 * Thanks to http://www.cplusplus.com/forum/general/85558/
 * for general idea on how to structure a dynamic queue
*/
class MemoQueue{

	private:
		struct node {
			Message data;
			node *next;		
		};
		
		FolderName name;
		node *beginning;
		node *end;
		int total;

	public:
		MemoQueue();
		~MemoQueue();

		void enqueue(Message m);
		Message dequeue();

		FolderName getFolderName();
		void setFolderName(FolderName folderName);
		int getTotal();

		void selfDestruct();
};
#endif
