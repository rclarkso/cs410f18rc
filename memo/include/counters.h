/*
 * The point of this file is to hold
 * external variables that will be changed throughout
 * the system. This will essentially be the "flag" system
 * that will tell us when the next memo needs to be sent
 */

extern int queuedMessages; // Stored the number of 'memos' in queue
extern int sentMessages; // Stores the number of messages sent 
extern bool  passedMessage; // T/F value to ensure that message was sent
extern int recievedMessage; // The number of messages that are going to be recieved
