#ifndef MEMOTABLE_H
#define MEMOTABLE_H

#include <FolderName.h>
#include <thread>
#include <mutex>
#include <Message.h>
#include <MemoQueue.h>
#include <string>
#include <iostream>
#include <string>
#include <functional> //for std::hash
#include <cstring>

using namespace std;

struct Bucket {
	int count;
	MemoQueue *queues;
};

class MemoTable {

	private:
		int count;
		Bucket *buckets;
		mutex mtx;				

		MemoQueue* get_queue(Bucket *bucket, FolderName key);

	public:
	
		MemoTable(int capacity);
		~MemoTable();

		void put(FolderName key,Message value);
		void put_delayed(FolderName key1,FolderName key2,Message value);
    
		void get(FolderName key, Message *message);
		Message get_copy(FolderName key);
		Message get_skip(FolderName key);
		Message get_copy_skip(FolderName key);
		Message get_alt(FolderName array_of_keys);
		Message get_alt_skip(FolderName array_of_keys);

		void free(Message m);
		unsigned long hash(string name);

		void selfdestruct();
};
#endif
