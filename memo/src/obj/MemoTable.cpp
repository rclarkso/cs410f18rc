#include <MemoTable.h>

using namespace std;

MemoTable::MemoTable(int capacity){
	count = capacity;
	buckets = new Bucket[count];
	for (int i = 0; i < count; i++)
		buckets[i] = Bucket();
}

MemoTable::~MemoTable(){  }

void MemoTable::get(FolderName key, Message *message){
	cout << "In get" << endl;
	mtx.lock();
	cout << "In in get " << endl;
	if (count == 0 or buckets == NULL) { 
		mtx.unlock();
		return; 
	}
	
	int index;
	Bucket *bucket;
	MemoQueue *queue;
	
	index = hash(key.name) % count;
	bucket = &(buckets[index]);	
	queue = get_queue(bucket,key);

	if (queue == NULL) { 
		mtx.unlock();
		return; 
	}
	
	*message = queue->dequeue();
	cout << "Getting " <<  message->data << endl;
	mtx.unlock();
	return;
}

void MemoTable::put(FolderName key, Message value){
	mtx.lock();
	int index;
	Bucket *bucket;
	MemoQueue *tempQueues, *queue;
		
	index = hash(key.name) % count;
	bucket = &(buckets[index]);	
	queue = get_queue(bucket,key);
	
	/** If the bucket isn't empty and has a matching foldername, enqueue the message */
	if (queue != NULL){
		(*queue).enqueue(value);
		cout << "Added memo to existing queue in bucket..." << endl;
		mtx.unlock();
		return;
	}	

	/** If the bucket is empty, initialize a queue and increment count */
	if (bucket->count == 0){
		cout << "Creating new queue in bucket..." << endl;
		bucket->queues = new MemoQueue[1];
		bucket->count = 1;
	}

	/** If the bucket is not empty (but null due to no matching foldername), add another queue to the bucket */	
	else {
		cout << "Adding new queue to bucket..." << endl;
		tempQueues = new MemoQueue[bucket->count + 1];
		for (int i = 0 ; i < bucket->count; i++)
			tempQueues[i] = bucket->queues[i];
		bucket->count++;
	}
	
	/** Get the newest (or first) queue, so the message can actually be enqueued */
	queue = &(bucket->queues[bucket->count-1]);
	*queue = *new MemoQueue();
	(*queue).setFolderName(key);
	(*queue).enqueue(value);
	cout << "Added memo to new queue... " << endl;
	mtx.unlock();
	return;
}

/** Gets an existing queue from given bucket */
MemoQueue* MemoTable::get_queue(Bucket *bucket, FolderName key){
	unsigned int i, n;
	MemoQueue *queue;
	string keyName = key.name;

	/** Return null if there is nothing in the bucket */
	if (bucket->count == 0) { 
		return NULL;
	}

	i = 0;
	n = bucket->count;
	/* Iterate through the given bucket's queues to see if there is one that matches the name/key they need */
	while (i < n){
		queue = &(bucket->queues[i]);
		string name = queue->getFolderName().name;
		if (!name.empty() && queue->getTotal() > 0){
			if (keyName.compare(name) == 0){
				return queue;
			}
		}
		i++;
	}
	/** Return null if nothing is found */
	return NULL;
}

unsigned long MemoTable::hash(string name){
	unsigned long hash = 5381;
    for (size_t i = 0; i < name.size(); ++i)
        hash = 33 * hash + (unsigned char)name[i];
    return hash;
}
