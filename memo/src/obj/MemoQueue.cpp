#include <MemoQueue.h>
#include <string>
#include <iostream>

using namespace std;

/**
 * http://www.cplusplus.com/forum/general/85558/
 * General idea on how to structure a dynamic queue
*/

MemoQueue::MemoQueue(){
    beginning = NULL;
    end = NULL;
    total = 0;
	name = FolderName();
}

MemoQueue::~MemoQueue(){
	selfDestruct();
}

/* Puts a message in a node at the end of the queue */
void MemoQueue::enqueue(Message message){
	node *newNode;
	newNode->data = message;
	newNode->next = NULL;

	/* If the queue is empty, the node is the start & the end */
	if (total == 0){
		beginning = newNode; 
		end = newNode;
	}
	else {
		end->next = newNode;
		end = newNode;
	}
	++total;
}

/* Removes the first node and returns its value in the given message parameter */
Message MemoQueue::dequeue(){
	node *tempNode;
	Message m;
	if (total != 0){
		m = beginning->data; // Store message in the parameter
		tempNode = beginning; // Set temporary node to the start (to be deleted later)
		beginning = beginning->next; // Set the new start to the second node
		--total; // Decrement count
	}
	return m;
}

void MemoQueue::selfDestruct(){
	// This will empty the queue by storing values in the temp message until there's none left (see dequeue method)
	while (total > 0){
		dequeue();
	}	
}

void MemoQueue::setFolderName(FolderName folderName){
	name = folderName;
}

FolderName MemoQueue::getFolderName(){
	return name;
}

int MemoQueue::getTotal(){
	return total;
}
