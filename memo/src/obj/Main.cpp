#include <MemoQueue.h>
#include <MemoTable.h>
#include <iostream>
#include <thread>

int main( int argc, const char* argv[] )
{
	Message m;
	m.data = 4;

	Message m2;
	m2.data = 6;

	Message m3;
	m3.data = 8;

	FolderName name;
	name.name = "Riley's Folder";

	FolderName nameTwo;
	nameTwo.name = "Ani's Folder";

	FolderName nameThree;
	nameThree.name = "Zac's Fodler";
	
	MemoTable *table;
	table = new MemoTable(100);

	Message *result;
	
	table->put(name,m);
	table->put(name,m2);
	table->put(name,m3);
	table->put(nameTwo,m);
	table->get(name,result);
	std::cout << "Result from folder is" << result->data << endl;	// Should be 4 (FIFO)

	/* Threading -- Not Working
	thread one(&MemoTable::put,
				table,
				name,m);
	
	thread two(&MemoTable::get,
				table,
				name,result);

	thread three(&MemoTable::put,
				table,
				nameTwo,m2);

	thread four(&MemoTable::put,
			table,
			name,m2);
	
	one.join();
	two.join();
	three.join();
	four.join();
	
	table->get(name,result);
	std::cout << "Result from folder is " << result->data << endl;
	*/
}
