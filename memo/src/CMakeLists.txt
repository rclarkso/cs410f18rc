cmake_minimum_required(VERSION 2.6)

set(include
	../include
)

set(obj
    ./obj/MemoQueue.cpp ./obj/MemoTable.cpp
)

set(testobj
    ./testobj/Memo_Tests.cpp
)

# Locate GTest
find_package(GTest REQUIRED)
include_directories(${GTEST_INCLUDE_DIRS} ${include})

# Link runTests with what we want to test and the GTest and pthread library
add_executable(Memo ./obj/Main.cpp ${obj})
target_link_libraries(Memo pthread)

add_executable(Test ./testobj/Memo_Main_Test.cpp ${testobj} ${obj})
target_link_libraries(Test ${GTEST_LIBRARIES} pthread)

