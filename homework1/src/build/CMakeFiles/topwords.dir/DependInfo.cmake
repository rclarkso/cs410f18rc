# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "C"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_C
  "/home/riley/cs410-f18-rc/homework1/src/obj/logic.c" "/home/riley/cs410-f18-rc/homework1/src/build/CMakeFiles/topwords.dir/obj/logic.c.o"
  "/home/riley/cs410-f18-rc/homework1/src/obj/main.c" "/home/riley/cs410-f18-rc/homework1/src/build/CMakeFiles/topwords.dir/obj/main.c.o"
  "/home/riley/cs410-f18-rc/homework1/src/obj/parsing.c" "/home/riley/cs410-f18-rc/homework1/src/build/CMakeFiles/topwords.dir/obj/parsing.c.o"
  "/home/riley/cs410-f18-rc/homework1/src/obj/strmap.c" "/home/riley/cs410-f18-rc/homework1/src/build/CMakeFiles/topwords.dir/obj/strmap.c.o"
  "/home/riley/cs410-f18-rc/homework1/src/obj/wordtracker.c" "/home/riley/cs410-f18-rc/homework1/src/build/CMakeFiles/topwords.dir/obj/wordtracker.c.o"
  )
set(CMAKE_C_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "/usr/local/include"
  "../../include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
