# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "C"
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_C
  "/home/riley/cs410-f18-rc/homework1/src/obj/logic.c" "/home/riley/cs410-f18-rc/homework1/src/build/CMakeFiles/tests.dir/obj/logic.c.o"
  "/home/riley/cs410-f18-rc/homework1/src/obj/parsing.c" "/home/riley/cs410-f18-rc/homework1/src/build/CMakeFiles/tests.dir/obj/parsing.c.o"
  "/home/riley/cs410-f18-rc/homework1/src/obj/strmap.c" "/home/riley/cs410-f18-rc/homework1/src/build/CMakeFiles/tests.dir/obj/strmap.c.o"
  "/home/riley/cs410-f18-rc/homework1/src/obj/wordtracker.c" "/home/riley/cs410-f18-rc/homework1/src/build/CMakeFiles/tests.dir/obj/wordtracker.c.o"
  )
set(CMAKE_C_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "/usr/local/include"
  "../../include"
  )
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/riley/cs410-f18-rc/homework1/src/testobj/logic_tests.cpp" "/home/riley/cs410-f18-rc/homework1/src/build/CMakeFiles/tests.dir/testobj/logic_tests.cpp.o"
  "/home/riley/cs410-f18-rc/homework1/src/testobj/parsing_tests.cpp" "/home/riley/cs410-f18-rc/homework1/src/build/CMakeFiles/tests.dir/testobj/parsing_tests.cpp.o"
  "/home/riley/cs410-f18-rc/homework1/src/testobj/strmap_tests.cpp" "/home/riley/cs410-f18-rc/homework1/src/build/CMakeFiles/tests.dir/testobj/strmap_tests.cpp.o"
  "/home/riley/cs410-f18-rc/homework1/src/testobj/topwords_maintest.cpp" "/home/riley/cs410-f18-rc/homework1/src/build/CMakeFiles/tests.dir/testobj/topwords_maintest.cpp.o"
  "/home/riley/cs410-f18-rc/homework1/src/testobj/wordtracker_tests.cpp" "/home/riley/cs410-f18-rc/homework1/src/build/CMakeFiles/tests.dir/testobj/wordtracker_tests.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "/usr/local/include"
  "../../include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
