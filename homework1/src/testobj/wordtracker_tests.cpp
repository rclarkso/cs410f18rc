#include <gtest/gtest.h>
#include "topwords_test.h"


/** Tests for WordTracker.c */

TEST(WordtrackerTest, NewWordtrackerTest){
	WordTracker *tracker = wordtracker_new();

	ASSERT_TRUE(tracker != NULL);
	ASSERT_EQ(tracker->how_many,10);
	ASSERT_EQ(tracker->last_n_words,1000);
	ASSERT_EQ(tracker->min_length,6);
	ASSERT_EQ(tracker->word_count,0);
	ASSERT_TRUE(tracker->hashmap != NULL);

}


