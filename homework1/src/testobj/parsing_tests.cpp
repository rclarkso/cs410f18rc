#include <gtest/gtest.h>
#include "topwords_test.h"

class ParsingTest : public ::testing::Test {
protected:
	WordTracker *test_tracker;
	
    void SetUp() override {
        test_tracker = wordtracker_new();
    };

    void TearDown() override {
    	wordtracker_delete(test_tracker);
	};
};

/** Tests for Parsing.c */

TEST_F(ParsingTest, ParseInputTest){
	parse_input(test_tracker);
	/** If read properly from stdin with fgetc, this should have 4 words in it now from test_input.txt (min length is 6 by default) */
	ASSERT_EQ(sm_get_count(test_tracker->hashmap),4);	
}

TEST_F(ParsingTest, SetGlobalVariablesTest){
	/** Tests that getopt_long properly sets/overrides default values for howmany, lastnwords, and minlength */
	int argc = 7;
	char * argv[7] = { "./topwords", "--howmany", "1", "--lastnwords", "1", "--minlength", "1" };

	set_global_variables(argc, argv, test_tracker);

	ASSERT_EQ(test_tracker->how_many,1);
	ASSERT_EQ(test_tracker->min_length,1);
	ASSERT_EQ(test_tracker->last_n_words,1);
}

TEST_F(ParsingTest, CheckIfBufferResetNeededTest){
	/** Buffer reset is only allowed if we're in between words (so we won't split up a word by accident),
	* if its not the end of the buffer,
	* and if we're close to the end. It can't be exact because we don't know where words/spaces will be.
	**/
	
	size_t number_read = 1920; // Number of chars read to the buffer so far
	size_t buffer_size = 2000; 
	char c = ' '; // Current character

	ASSERT_TRUE(buffer_reset_needed(number_read,buffer_size,c) == true);

	number_read = 1800;
	ASSERT_FALSE(buffer_reset_needed(number_read,buffer_size,c) == true);

	number_read = 1920;
	c = 'm';
	ASSERT_FALSE(buffer_reset_needed(number_read,buffer_size,c) == true);
} 

TEST_F(ParsingTest, ResetBufferTest){
	size_t length = 2000 * sizeof(char); // Same size used in program
	char *test_buffer = (char*) malloc(length);

	for (int i = 0; i < length; i++){
		test_buffer[i] = 'm'; // Setting content to something random, we just need to see that it empties
	}

	reset_buffer(test_tracker,test_buffer,length);

	for (int i = 0; i < length; i++){
		ASSERT_TRUE(test_buffer[0] == 0); // The buffer should be reset with 0s
	}
}
 
TEST_F(ParsingTest, CheckInvalidCharTest){
	char invalid_chars[11] = {'"', '\n', '\r', '!', '?', '.', ',', '_', ';', '-', '/'};
	
	for (int i = 0; i < 11; i++){
		ASSERT_TRUE(is_valid_character(invalid_chars[i]) == false); // Assert that character is invalid
	}    
}
