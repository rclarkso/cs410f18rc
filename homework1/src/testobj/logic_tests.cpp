#include <gtest/gtest.h>
#include "topwords_test.h"


class LogicTest : public ::testing::Test {
protected:
	WordTracker *test_tracker;
	const char* key = "test";

    void SetUp() override {
        test_tracker = wordtracker_new();
    };

    void TearDown() override {
    	wordtracker_delete(test_tracker);
	};
};

/** Tests for Logic.c */

TEST_F(LogicTest, UpdateTopwordsTest){
	/** Insert 10 (n) words with different frequencies */
	const char *test_words[10] = { "one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten"};
	for (int i = 0; i < 10; i++){
		long int val = i+5;
		const long int *ptr = &val;
		sm_put(test_tracker->hashmap,test_words[i],ptr);
	}
	/** Create arrays for top n values and keys */
	char *top_n_words[test_tracker->how_many];
	long int top_n_values[test_tracker->how_many];
	update_top_words(test_tracker,top_n_words,top_n_values);
	/** Check that the most frequent is ten, with value 14 (i + 5) */
	ASSERT_TRUE(strcmp(top_n_words[0],test_words[9]) == 0);
	ASSERT_EQ(top_n_values[0],14);
}

TEST_F(LogicTest, InitialInsertTest){
	initial_insert(test_tracker,key);
	int result = sm_get(test_tracker->hashmap, key);
	ASSERT_EQ(result,1);
}

TEST_F(LogicTest, IncrementWordTest){
	/** First insert value */
	initial_insert(test_tracker,key);
	int result = sm_get(test_tracker->hashmap, key);
	/** This should increment existing value by 1 */
	increment_word(test_tracker,key,result);
	int incrementedResult = sm_get(test_tracker->hashmap,key);

	ASSERT_EQ(incrementedResult,2);
}

TEST_F(LogicTest, StoreWordTest){
	/** Test that words can be stored in the hashmap via this function */
	const char word[] = "word";
	store_word(test_tracker,word);
	ASSERT_TRUE(sm_exists(test_tracker->hashmap,word) == 1);
}

TEST_F(LogicTest, UpdateCheckTest){
	/** Should reset current word count to 0 if we are at n words (which is 1000 by default) */
	test_tracker->word_count = 1000;
	update_check(test_tracker);
	ASSERT_EQ(test_tracker->word_count, 0);
}

TEST_F(LogicTest, ProcessWordsTest){
	/** Test by looking at word count and whether word is inserted into the hashmap */
	
	char test_words[] = "hello world this is a test minimum length";
	
	process_words(test_tracker,test_words);
	ASSERT_EQ(test_tracker->word_count,2);
	
	int has_word = sm_exists(test_tracker->hashmap,"minimum");
	ASSERT_EQ(has_word,1);

	has_word = sm_exists(test_tracker->hashmap,"length");
	ASSERT_EQ(has_word,1);

	has_word = sm_exists(test_tracker->hashmap,"a");
	ASSERT_EQ(has_word,0);
}
