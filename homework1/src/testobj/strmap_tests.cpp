#include <gtest/gtest.h>
#include "topwords_test.h"

class StrMapTest : public ::testing::Test {
protected:
	WordTracker *test_tracker;
	const char* key = "test";
	long int addto_val = 5;
   	const long int *ptr = &addto_val;
    
	void SetUp() override {
        test_tracker = wordtracker_new();
		/** Insert test value */

		sm_put(test_tracker->hashmap,key,ptr);
    };

    void TearDown() override {
    	wordtracker_delete(test_tracker);
	};
};

/** Tests for StrMap.c */

TEST_F(StrMapTest, StrMapGetPairTest){	
	int index = hash(key) % test_tracker->hashmap->count;
	Bucket *bucket = &(test_tracker->hashmap->buckets[index]);

	Pair *result = get_pair(bucket,key);
	ASSERT_EQ(*result->value,5);
	ASSERT_EQ(0,strcmp(result->key,key));
}

TEST_F(StrMapTest, StrMapGetMaxTest){

	/** Insert extra values to see that maxes can be retrieved successfully */
	addto_val = 4;
	sm_put(test_tracker->hashmap,"testtwo",ptr);
	addto_val = 2;
	sm_put(test_tracker->hashmap,"testthree",ptr);

	Pair *firstMax = sm_getmax(test_tracker->hashmap,0); // gets highest max
	ASSERT_EQ(*firstMax->value,5);
	
	Pair *secondMax = sm_getmax(test_tracker->hashmap,*firstMax->value); //gets highest value that is less than given second parameter
	ASSERT_EQ(*secondMax->value,4);

	Pair *thirdMax = sm_getmax(test_tracker->hashmap,*secondMax->value);
	ASSERT_EQ(*thirdMax->value,2);
}

TEST_F(StrMapTest, StrMapCountTest){
	ASSERT_EQ(test_tracker->hashmap->count, 1000); // Should be initialized to 10,000
}

TEST_F(StrMapTest, StrMapPutTest){
	ASSERT_EQ(sm_put(test_tracker->hashmap,NULL,ptr),0); // Null key should == 0
	ASSERT_EQ(sm_put(test_tracker->hashmap,"test two",NULL),0); // Null value should == 0
	ASSERT_EQ(sm_put(test_tracker->hashmap,"test three",ptr),1); // Valid key/value == 1
}

TEST_F(StrMapTest, StrMapExistsTest){
	int exists = sm_exists(test_tracker->hashmap,key);
	ASSERT_EQ(exists,1); // Key inserted at setup() should exist, == 1
	
	exists = sm_exists(test_tracker->hashmap, "shouldNotExist");
	ASSERT_EQ(exists,0); // This key should not exist, == 0
}

TEST_F(StrMapTest, StrMapGetTest){
	long int value = sm_get(test_tracker->hashmap,key); // Should return a value (5)
	long int other_value = sm_get(test_tracker->hashmap, "shouldNotExist"); // Should return -1 bc it doesnt exist

	ASSERT_EQ(value, (long int) 5);
	ASSERT_EQ(other_value, (long int) -1);
}

TEST_F(StrMapTest, NewStrMapTest){
	StrMap *map = sm_new(5);
	
	ASSERT_TRUE(map != NULL);	
	ASSERT_EQ(map->count,5);
	ASSERT_TRUE(map->buckets != NULL);
}
