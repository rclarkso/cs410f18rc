#include <parsing.h>

bool buffer_reset_needed(size_t number_read, size_t buffer_size, char c);
bool is_valid_character(char c);
void reset_buffer(WordTracker *wordtracker, char* buffer, size_t buffer_size);
void set_global_variables(int argc, char *argv[], WordTracker *wordtracker);
void parse_input(WordTracker *wordtracker);

/** Methods **/

void set_global_variables(int argc, char * argv[], WordTracker *wordtracker){

   char c;
   const char * short_options = "h:m:w:";
   /**
    * https://stackoverflow.com/questions/39966025/understanding-option-long-options-when-using-getopt-long
    * Question answered how to get a program to recognize arguments longer than a character.
   **/
   struct option long_options[] =
   {
      {"lastnwords", required_argument, NULL, 'w'},
      {"minlength", required_argument, NULL, 'm'},
      {"howmany", required_argument, NULL, 'h' },
	  { 0,	0,	0,	0} 
   };
   
   /**
	*  https://stackoverflow.com/questions/16573329/assigning-optarg-to-an-int-in-c
    *  Question answered how to define variables with optarg value (atoi() function).
    *  https://stackoverflow.com/questions/39337345/program-in-c-to-read-arguments
    *  Question answered how to read options/arguments passed in when the program was executed.
   **/
   while((c = getopt_long(argc, argv, short_options, long_options, NULL)) != -1)
   {
      switch(c)
      {		 
         case 'h':
         wordtracker->how_many = atoi(optarg);
         break;

         case 'm':
		 wordtracker->min_length = atoi(optarg);
         break;

         case 'w':
		 wordtracker->last_n_words = atoi(optarg);
		 break;

         default:
      	 printf("Did not recognize command. Please retry with format: './topwords --howmany x --lastnwords y --minlength z\n");
		 exit(0);
		}
   }
}

/**
 * https://stackoverflow.com/questions/2505451/how-to-read-unlimited-characters-in-c 
 * This question helped me figure out how to read in words via a buffer. Also relevant for the reset_buffer function.
**/
void parse_input(WordTracker *wordtracker){

	char *buffer;
	size_t number_read;
	size_t buffer_size;

	buffer_size = 2000 * (sizeof(char));
	buffer = (char*) malloc(buffer_size);
	number_read = 0;
	
	char c;

	while ((c = fgetc(stdin)) != EOF){
		
		/** Start a new buffer, if necessary, at a break in between words towards the end of the current buffer */
		if (buffer_reset_needed(number_read, buffer_size, c)){

			reset_buffer(wordtracker,buffer,buffer_size);
			/** Set first character to a space to prevent joined words in between buffers */
			buffer[0] = ' ';
			number_read = 1;
		} 	

		/** Read in character so long as it is valid */
		else if (is_valid_character(c)){
			buffer[number_read] = tolower(c);
        	number_read++;
		}

		/** If character is invalid, replace it with a space, just to be sure that the words will be split correctly */
		else if (!is_valid_character(c)){
			c = ' ';
			buffer[number_read] = c;
			number_read++;
		}		
	}
	if ( c == EOF ){
		process_words(wordtracker,buffer);
	}

	free(buffer);
}

bool buffer_reset_needed(size_t number_read, size_t buffer_size, char c){
	bool is_near_end = number_read > buffer_size - 100;
	bool is_not_end = number_read <= buffer_size;
	bool is_inbetween_words = c == ' ';
	return is_near_end && is_not_end && is_inbetween_words;
}

/** See comments on above function for credit */
void reset_buffer(WordTracker *wordtracker, char *buffer, size_t buffer_size){
	process_words(wordtracker,buffer);

	memset(buffer, 0, buffer_size);	

	char *new_buffer;
	new_buffer = (char*) realloc(buffer, buffer_size);	
	buffer = new_buffer;
}

/**
 * https://stackoverflow.com/questions/1071542/in-c-check-if-a-char-exists-in-a-char-array
 * Shows how to compare a char to a list of invalid chars
**/
bool is_valid_character(char c){
 	const char *invalidCharacters = "\n_,\"\'\?!./\r;-“”";
	char *currentChar = (char*)  malloc(sizeof(char));
	*currentChar = c;
	return (strchr(invalidCharacters, *currentChar)) ? false : true;
}
