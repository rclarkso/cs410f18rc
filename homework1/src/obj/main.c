#include <mainheader.h>

static void iter(const char *key, const long int *value, const void *obj);
void print_map(void);

int main(int argc, char *argv[]) {

	/* Declare global variables */
	WordTracker *wordtracker = wordtracker_new();

	/** Found this on stackoverflow  https://stackoverflow.com/questions/18935446/program-received-signal-sigpipe-broken-pipe shows how to ignore sigpipe */
	signal(SIGPIPE, SIG_IGN);
	
	/** Set global values by default or based on input : parsing.c */
	set_global_variables(argc,argv,wordtracker);

	printf("\nMinlength: %d  Howmany: %d  LastNWords: %d\n\n", wordtracker->min_length, wordtracker->how_many, wordtracker->last_n_words);

	/** Parse the input from stdin : parsing.c */
	parse_input(wordtracker);
	
	/** Delete the hashmap */
	sm_delete(wordtracker->hashmap);
	
	return(0);
}

/** This function can be used for testing by iterating through and printing the hashmap key value pairs */
static void iter(const char *key, const long int *value, const void *obj)
{	
    printf("key: %s value: %ld\n", key, *value);
}

