#include <wordtracker.h>

WordTracker *wordtracker_new(void);

WordTracker * wordtracker_new(){

    WordTracker *wordtracker;

    wordtracker = malloc(sizeof(int)*4);

    if (wordtracker == NULL){
        return NULL;
    }

    /** Defualt values */
    wordtracker->last_n_words = 1000;
    wordtracker->how_many = 10;
    wordtracker->min_length = 6;

    /** Initialize global word count to 0 and initialize hashmap */
	wordtracker->word_count = 0;
	wordtracker->hashmap = sm_new(1000); /** Store up to 10,000 unique key-value pairs */

	if (wordtracker->hashmap == NULL) {
        printf("\nHashmap allocation failure.\n");
        exit(0);
    }

    return wordtracker;
}

void wordtracker_delete(WordTracker *wordtracker){
	sm_delete(wordtracker->hashmap);
	free(wordtracker);
}
