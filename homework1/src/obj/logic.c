#include <logic.h>

/** Methods */
void process_words(WordTracker *wordtracker, char *characters);
void store_word(WordTracker *wordtracker, const char *word);
void update_check(WordTracker *wordtracker);
void increment_word(WordTracker *wordtracker, const char *word, int result);
void initial_insert(WordTracker *wordtracker, const char *word);
void update_top_words(WordTracker *wordtracker, char *top_n_words[], long int top_n_values[]);
void print_top_words(int length, char *top_n_words[], long int top_n_values[]);
void update_check(WordTracker *wordtracker);

/**
 * https://stackoverflow.com/questions/4513316/split-string-in-c-every-white-space
 * Question answered how to split a long string into words by a delimiting character.
 * https://stackoverflow.com/questions/1088622/how-do-i-create-an-array-of-strings-in-c
 * Question showed how to store strings in an array.
**/
void process_words(WordTracker *wordtracker, char *characters){
	char *split = strtok(characters," ");	
	int i = 0;

	/** Go through buffer (characters) and split into words by spaces */
	while (split != NULL){
		/** Store word in map if its an acceptable length */
		if (strlen(split) >= wordtracker->min_length){
			wordtracker->word_count++;
			store_word(wordtracker,split);
			i++;
		}
		update_check(wordtracker);
		split = strtok(NULL, " ");
	}
}

void update_check(WordTracker *wordtracker){
	/** If we are at the limit of LastNWords, publish an update and restart word count */
	if (wordtracker->word_count == wordtracker->last_n_words){
    	wordtracker->word_count = 0;

		char *top_n_words[wordtracker->how_many];
		long int top_n_values[wordtracker->how_many];
		
    	update_top_words(wordtracker,top_n_words,top_n_values);
		print_top_words(wordtracker->how_many,top_n_words,top_n_values);
    	sm_delete(wordtracker->hashmap);
		wordtracker->hashmap = sm_new(1000);
	}
}

/**
 * https://stackoverflow.com/questions/26597977/split-string-with-multiple-delimiters-using-strtok-in-c
 * Question shows how to delimit based on mutliple characters in C
**/
void store_word(WordTracker *wordtracker, const char *word){

	int result = sm_get(wordtracker->hashmap, word);

    /** Word is not in hashmap; insert key value pair initialized to value of 1 */
	if (result == -1){
		initial_insert(wordtracker,word);
	}

    /** Word is in hashmap; insert current value + 1 */
	else {
		increment_word(wordtracker,word,result);
    }
}

void increment_word(WordTracker *wordtracker, const char *word, int result){
	long int addto_val = result+1;
	const long int *ptr = &addto_val;        	
	sm_put(wordtracker->hashmap, word, ptr);	
}

void initial_insert(WordTracker *wordtracker, const char *word){
	long int start_val = 1;
	const long int *ptr = &start_val;
	sm_put(wordtracker->hashmap, word, ptr);
}

/** Get the n max value pairs and print */
void update_top_words(WordTracker *wordtracker, char *top_n_words[], long int top_n_values[]){
	if (sm_get_count(wordtracker->hashmap) >= wordtracker->how_many){
		
		long int max = 0;

		for (long int i = 0; i < wordtracker->how_many; i++){
			Pair *pair = sm_getmax(wordtracker->hashmap,max);
			max = *pair->value;

			top_n_words[i] = pair->key;
			top_n_values[i] = *pair->value;		
		}
	}
}

void print_top_words(int length, char *top_n_words[], long int top_n_values[]){
	if (top_n_words[0] != NULL && top_n_values[0] != 0){
		for (long int i = 0; i < length; i++){
			printf("%s: %ld  ",top_n_words[i], top_n_values[i]);
			if (i + 1 == length){	
				printf("\n\n");
			}
		}
	}	
}
