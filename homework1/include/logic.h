#ifndef _LOGIC_H_
#define _LOGIC_H_

#include <wordtracker.h>
#include <strmap.h>
#include <stdio.h>

/** Methods */
extern void update_top_words(WordTracker *wordtracker, char * top_n_words[], long int top_n_values[]);
extern void process_words(WordTracker *wordtracker, char *characters);
extern void store_word(WordTracker *wordtracker, const char *word);
extern void update_check(WordTracker *wordtracker);
extern void initial_insert(WordTracker *wordtracker, const char *word);
extern void increment_word(WordTracker *wordtracker, const char *word, int result);

#endif
