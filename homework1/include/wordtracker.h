#ifndef _WORDTRACKER_H_
#define _WORDTRACKER_H_

#include <strmap.h>
#include <stdio.h>

typedef struct WordTracker WordTracker;

struct WordTracker {
     int last_n_words;
     int how_many;
     int min_length;
     int word_count;
     StrMap *hashmap;
};

extern WordTracker *wordtracker_new(void);
extern void wordtracker_delete(WordTracker *wordtracker);

#endif
