#ifndef _PARSING_H_
#define _PARSING_H_

#include <ctype.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <getopt.h>
#include <wordtracker.h>
#include <logic.h>

extern void parse_input(WordTracker *wordtracker);
extern void set_global_variables(int argc, char * argv[], WordTracker *wordtracker); // Sets global variables
extern bool is_valid_character(char c);
extern void reset_buffer(WordTracker *wordtracker, char *buffer, size_t buffer_size);
extern bool buffer_reset_needed(size_t number_read, size_t buffer_size, char c);
#endif
