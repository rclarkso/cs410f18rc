# Operating Systems

Fall 2018 Operating Systems Course

## General Notes
Please excuse no commits on Tuesdays/Wednesdays/Thursdays. I work from 8-5 those days and then usually have night class from 7-9:30. Basically gone from 7am-10pm and have no time to put in work.

## Memo

Ani and I are working together on Memo. 

It's not going to be finished in time for the final unfortunately, but here's some info on how it went:

- I think the MemoQueue and MemoTable classes are good, they seem pretty organized and work well together
- I included a lot of functions in the header files that didn't get created (but were mentioned in the memo paper), this was just to help make the roadmap in the beginning
- I didn't dedicate as much time to this class the latter half of the semester because I put in a lot of time the first half
- Most of our time was spent developing the queue and the table structures and trying to figure out what memo is exactly / how things were supposed to work
- The queue and table took a long time because I did not use a queue for the first homework, and had to adapt the table I used for the first homework to C++ and to fit memo's needs
- We got to start messing with threads/mutexes towards the end, but nothing with them works
- The table of queues seems to work without the threads
- If you look in Main.cpp, I will comment out the section of code that uses threads so you can see what we were trying, but will leave the regular code uncommented because it actually works
- This is not an A project but I definitely un-rustified my C/C++ skills this semester and learned a lot about threading, so I'm not unhappy

### Instructions

Go to the memo/src/build directory and run ./Memo to execute the main method

It's just a simple example of the structure working

You can look inside Main.cpp to see the example and the (commented out) attempt at using threads

## Homework 1B

Check out branch 1B for current commits!

### Notes

First: I only have 19 tests. I could have gotten to 25 if I really felt the need to, but my program is relatively small and already pretty broken up into functions. Instead of writing 6 more tests, I decided to organize the test directory structure better and make my existing tests better. I felt that writing more tests would not have added value (unless I went out of my way to make more features for my program -- which I would have liked to do if I had the time, but did not).

A good bit of refactoring/improvements went into this. Instead of using global variables to keep track of the number of words, how many, last n words, etc... I turned it into a type: wordtracker. This contains all the global variables I was keeping track of, including the hashmap, and its also possible to pass from function to function which makes testing easier. I did break up some functions within the logic/parsing files. 

I also cleaned up my header files. Originally I had the obj header files inheriting from the main header file, but I realized it should be the reverse. Now main header and main.c takes from the other files, not the other way around. Each file also now has its own header. Its a bit cleaner.

In order to test I had to take any dependencies out of main as the googletest main can't test another main... so main.c has been cleaned up and there's less in there. Any functionality was taken out into wordtracker.c/wordtracker.h or to logic.c/logic.h.

There's a new project structure. /src contains /testobj and /obj files. Executables are made within /build and then output into /build/bin. Test inputs are in the /input directory. CMakeLists.txt is in /src.

### Regular Program Instructions

Same thing but with Cmake now. If you want to build the executables, see the instructions for building the tests as it will build both the test and regular program executable.

To run the normal program in the new project structure:

` cd /src `

` ./build/bin/topwords < input/prideandprejudice.txt `

### Testing Instructions

There is now a /src directory that contains a /build directory for cmake things and objects for tests under /testobj as well as preexisting obj files under /obj. I left the orignal /src directory even though an executable is built for topwords with cmake within the test/build directory as well. The following should run the tests (I've pushed to bitbucket the built executable):

` cd /src `

` ./build/bin/tests < input/test_input.txt `

* THE TESTS WILL ONLY ALL PASS IF YOU USE THE TEST INPUT LIKE ABOVE. Only one depends on it, but it expects input from stdin and the other tests will not run if that one waits for input forever...

If you want to try building the tests first:

` cd /src `

` rm -rf /build/* `

` cd build `

` cmake .. `

` make `

Then cd back to /src and repeat the first round of commands.

If there are errors, you may need to build/install googletest. Ensure that git has pulled the googletest directory under /cs410f18rc :

` git pull --recurse-submodules `

OR if its the first time you're checking out the submodule -

` git submodule update --init --recursive `

From there follow George's original instructions:

` cd googletest `

` mkdir build `

` cd build `

` cmake .. `

` make `

` sudo make install `

Then try going to the /src directory. Do:

` rm -rf /build/* `

` cd /build `

and then repeat the first round of commands.

### Untested Functions

* Deletion functions are not tested. I don't know how to test that something was freed.

* Sm_enum in StrMap is not tested because I do not use it (but it came with the implementation). 

* I do not test the print function in logic because it's not that important to the logic of the program.

## Homework 1A

### Extra Credit Features
* Case Insensitivity

### Notes

I decided to go with the top comment's technique listed at this stackoverflow question https://stackoverflow.com/questions/185697/the-most-efficient-way-to-find-top-k-frequent-words-in-a-big-word-sequence. Things ended up changing some, but I tried to follow the idea.

I used a simplified hashmap implementation. I started with this published implementation http://pokristensson.com/strmap.html. As you can see in my code, I did change the implementation to be a map of strings to long ints instead of strings to strings. The original comments and the GNU Lesser General Public License are available in src/obj/strmap.c and include/strmap.h . It required some tweaking and adding a new function to get to work (and this was a lot of work for me who hasn't used C in two years). Functionally, I realize the name strmap is no longer appropriate, but I didn't want to rename it and seem like I was taking credit.

I give credit to all important stackoverflow questions. See comments in .c and .h files. If something comes up that's not documented I'm sorry and I will totally find it in my internet history if you ask!!

### Limitations

* I use a long int. Obviously this comes to a limit at some point. Realistically, a fix for that situation could be to simplify all current values proportionally and count from those smaller numbers... then repeat etc.

* Character limit for a word is 30 characters. Okay, realistically, I think its completely unlikely that a FREQUENT word would be 30+ characters. So I'm not sure this really matters.

* Hashmap can only hold up to 10,000 unique words that are greater than the min length limit. 

* Not capable of running in constant time. I couldn't figure that out, but I did try. Honestly, I think with more time I could have done it but I have a pretty tight schedule. I imagine it can't be constant because of the long int and the 10,000 unique word limitations. However, it does work on large files like the prideandprejudice.txt file I have in the directory for testing. 

### Instructions

cd into the /homework1/src directory

`'make'` should create the necessary executable

`'./topwords --howmany x --lastnwords y --minlength z'` will run the program with the set options

`'./topwords'` will run the program with default options

`'./topwords < prideandprejudice.txt'` will run the program on the given file. If you want to use your own text file, put it in the homework1/src directory or give the full filepath in the command 

if you don't give a file as input: when the program runs, you can paste or type whatever you want, then press ctrl+d to process the input (sometimes you gotta do it twice)

### How to Break My Program

1. paste in a lot of stuff instead of using a text file

2. do something not listed in instructions

3. try to get it to work in constant time

4. give it a weird file




